var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('site_index', { title: 'Página inicial', site: 'N', sitee: 'Servicos' });
});

router.get('/creator', function(req, res, next) {
  res.render('site_creator', { title: 'Creator' });
});

module.exports = router;
